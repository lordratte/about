Hi, there :wave:

I'm glad you could join me. I'm an old Python dev and new Rust dev always on the lookout for cool stuff to build.

# Some Projects

- [TishlyWiki](https://gitlab.com/lordratte/tishlywiki) (`Bash`,`Powershell`,`Batch`,`Python`)

    Don't try this at home. An html file that behaves like a cross-plaform binary using ambiguity between programming languges and systems.

- [Project Euler Solutions](https://gitlab.com/lordratte/project-euler-rust) (`Rust`)

    Solutions written in Rust for mathematical challenges in the [Project Euler](https://projecteuler.net/) Database.

- [Piped server handler](https://gitlab.com/lordratte/tishlywiki-handler-rust) (`Rust`)

    This is a helper for TishlyWiki that behaves like listen-mode on socat, ncat, or nc. I didn't really understand what I was doing so it could probably use a revision.

- [Axiomatic](https://gitlab.com/lordratte/axiomatic) (`Python`)

    A fuzzy logic/propositional logic library that I've used in the REPL for certain problems but would like to still use in an application.

- [TiddlyEdit](https://gitlab.com/lordratte/tiddly-edit-android) (`Kotlin`)

    An Android app that would let you edit TiddlyWiki files using native Android components rather than using a slow webview. This didn't really go anywhere.

- [Screeps "AI"](https://gitlab.com/lordratte/screeps-strats) (`JavaScript`)

    A brain for the programming MMO screeps. It was my first attempt. Pretty cringy stuff.

- [Python Dictionary Mounted as Directory](https://gitlab.com/lordratte/python-dictionary-fuse) (`Python`)

    Messing around with Linux's fuse.
